<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE article>
<article
  xmlns="http://docbook.org/ns/docbook" version="5.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  lang="en" >
  <articleinfo>
    <title>Shortcuts Specification</title>
    <releaseinfo>0.1 (DRAFT)</releaseinfo>
    <pubdate>2022-05-11</pubdate>
    <authorgroup>
      <author>
	<firstname>Aleix Pol</firstname>
	<surname>Gonzalez</surname>
	<affiliation>
	  <address>
	    <email>aleixpol@kde.org</email>
	  </address>
	</affiliation>
      </author>
    </authorgroup>
  </articleinfo>

  <section xml:id="about">
    <title>About</title>
    <para>
      The purpose of this specification is to specify how shortcuts
      can be specified between components. Be it for global shortcuts
      in the XDG portals or to specify how we need an application to
      get started, we need shared means to communicate these triggers.
    </para>
    <para>
      This document contemplates simple keyboard shortcuts right now.
      Extending this specification with more complex cases or input
      beyond the traditional keyboard devices is something that can be
      included in future versions of the spec.
    </para>
    <para>
      The identifiers used in this document all come from the
      xkbcommon project, used by most implementations of keyboard
      input. You can find more information here: https://xkbcommon.org
    </para>
  </section>
  <section xml:id="specification">
    <title>Specification</title>
    <para>
      A shortcut is comprised by a set of modifiers (namely CTRL, ALT,
      SHIFT, NUM and LOGO, as defined as XKB_MOD_NAME_* in
      <link xlink:href="https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-names.h">xkbcommon-names.h</link>)
      together with a key identifier, joined by a + sign.
    </para>
    <para>
      Identifiers are taken from
      <link xlink:href="https://github.com/xkbcommon/libxkbcommon/blob/master/include/xkbcommon/xkbcommon-keysyms.h">xkbcommon-keysyms.h</link>
      without the XKB_KEY_* prefix. Identifiers only contain
      alphanumerical values and underscore (_).
    </para>
    <para>
      Examples: * a: Signifies the <literal>XKB_KEY_a</literal> key on
      a keyboard * CTRL+a: <literal>Control</literal> modifier is
      pressed, then the <literal>XKB_KEY_a</literal> key *
      CTRL+SHIFT+a: <literal>Control</literal> and
      <literal>Shift</literal> modifiers are pressed, then the
      <literal>XKB_KEY_a</literal> key. * CTRL+ALT+Return:
      <literal>Control</literal> and <literal>Alt</literal> modifiers
      are pressed, then the <literal>XKB_KEY_Return</literal> key.
    </para>
    <para>
      Here we are defining the language to communicate a desired
      shortcut between components in a standard way. It’s up to the
      implementation to produce the expected behaviour to their users.
      The implementation will be responsible for the effort of
      translating the entered shortcut into something that can be
      expressed in the user’s keyboard layout.
    </para>
    <para>
      Limit shortcuts to the base (or depressed, in terms of
      xkbcommon’s documentation) layer, so it’s easy to understand
      what it refers to. What a base layer is will depend on the
      specific layout in use, it’s up to the implementation to decide
      how to resolve these.
    </para>
    <para>
      Shortcuts will be parsed until an undocumented character is
      found.
    </para>
  </section>
</article>
